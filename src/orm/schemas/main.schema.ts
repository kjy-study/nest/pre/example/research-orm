import { MAIN } from '@prisma/client';

export class MainSchema implements MAIN {
  dataId: string;
  name: string;
}
