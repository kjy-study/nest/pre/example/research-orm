import { SUB_5 } from '@prisma/client';

export class Sub5Schema implements SUB_5 {
  dataId: string;
  mainDataId: string;
}
