import { SUB_3 } from '@prisma/client';

export class Sub3Schema implements SUB_3 {
  dataId: string;
  mainDataId: string;
}
