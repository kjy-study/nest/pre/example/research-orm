import { SUB_4 } from '@prisma/client';

export class Sub4Schema implements SUB_4 {
  dataId: string;
  mainDataId: string;
}
