import { SUB_2 } from '@prisma/client';

export class Sub2Schema implements SUB_2 {
  dataId: string;
  mainDataId: string;
}
