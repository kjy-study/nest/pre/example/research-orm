import { SUB_1 } from '@prisma/client';

export class Sub1Schema implements SUB_1 {
  dataId: string;
  mainDataId: string;
}
