import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Sub1Entity } from './sub-1.entity';
import { Sub2Entity } from './sub-2.entity';
import { Sub3Entity } from './sub-3.entity';
import { Sub4Entity } from './sub-4.entity';
import { Sub5Entity } from './sub-5.entity';

@Entity({ name: 'MAIN' })
export class MainEntity {
  @PrimaryGeneratedColumn('uuid', { name: 'data_id' })
  data_id: string;

  @Column({ name: 'name' })
  name: string;

  @OneToMany((type) => Sub1Entity, (sub1) => sub1.main)
  sub1: Sub1Entity[];

  @OneToMany(() => Sub2Entity, (sub2) => sub2.main)
  sub2: Sub2Entity[];

  @OneToMany(() => Sub3Entity, (sub3) => sub3.main)
  sub3: Sub3Entity[];

  @OneToMany(() => Sub4Entity, (sub4) => sub4.main)
  sub4: Sub4Entity[];

  @OneToMany(() => Sub5Entity, (sub5) => sub5.main)
  sub5: Sub5Entity[];
}
