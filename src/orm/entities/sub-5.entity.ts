import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { MainEntity } from './main.entity';

@Entity({ name: 'SUB_5' })
export class Sub5Entity {
  @PrimaryGeneratedColumn('uuid', { name: 'data_id' })
  data_id: string;

  @Column({ name: 'main_data_id' })
  main_data_id: string;

  @ManyToOne((type) => MainEntity, (main) => main.sub5)
  @JoinColumn({ name: 'main_data_id', referencedColumnName: 'data_id' })
  main: MainEntity;
}
