import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MainEntity } from './entities/main.entity';

@Injectable()
export class TypeOrmRepository {
  constructor(
    @InjectRepository(MainEntity)
    private readonly ormRepository: Repository<MainEntity>,
  ) {}

  private readonly dataId = '76f6b046-abb3-11ed-85c8-0242ac190002';

  async findOne() {
    const main = await this.ormRepository.findOne({
      where: {
        data_id: this.dataId,
      },
    });
    return main;
  }
  async relSub1() {
    const main = await this.ormRepository.findOne({
      where: {
        data_id: this.dataId,
      },
      relations: {
        sub1: true,
      },
      relationLoadStrategy: 'query', // default: join
    });

    return main;
  }

  async relSub2() {
    const main = await this.ormRepository.findOne({
      where: {
        data_id: this.dataId,
      },
      relations: {
        sub1: true,
        sub2: true,
      },
      relationLoadStrategy: 'query', // default: join
    });

    return main;
  }
  async relSub3() {
    const main = await this.ormRepository.findOne({
      where: {
        data_id: this.dataId,
      },
      relations: ['sub1', 'sub2', 'sub3'],
      relationLoadStrategy: 'query', // default: join
    });

    return main;
  }
  async relSub4() {
    const main = await this.ormRepository.findOne({
      where: {
        data_id: this.dataId,
      },
      relations: ['sub1', 'sub2', 'sub3', 'sub4'],
      relationLoadStrategy: 'query', // default: join
    });

    return main;
  }
  async relSub5() {
    const main = await this.ormRepository.findOne({
      where: {
        data_id: this.dataId,
      },
      relations: ['sub1', 'sub2', 'sub3', 'sub4', 'sub5'],
      relationLoadStrategy: 'query', // default: join
    });

    return main;
  }
}
