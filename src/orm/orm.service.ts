import { Injectable } from '@nestjs/common';
import { PrismaRepository } from './prisma.repository';
import { TypeOrmRepository } from './type-orm.repository';

@Injectable()
export class OrmService {
  constructor(
    private readonly ormRepository: TypeOrmRepository,
    private readonly prismaRepository: PrismaRepository,
  ) {}

  async findOneMain() {
    return await this.prismaRepository.findOne();
  }

  async findOneMainRelSub1() {
    return await this.prismaRepository.relSub1();
  }

  async findOneMainRelSub2() {
    return await this.prismaRepository.relSub2();
  }

  async findOneMainRelSub3() {
    return await this.prismaRepository.relSub3();
  }

  async findOneMainRelSub4() {
    return await this.prismaRepository.relSub4();
  }

  async findOneMainRelSub5() {
    return await this.ormRepository.relSub5();
  }
}
