import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PrismaModule } from '../config/database/prisma.module';
import { MainEntity } from './entities/main.entity';
import { Sub1Entity } from './entities/sub-1.entity';
import { Sub2Entity } from './entities/sub-2.entity';
import { Sub3Entity } from './entities/sub-3.entity';
import { Sub4Entity } from './entities/sub-4.entity';
import { Sub5Entity } from './entities/sub-5.entity';
import { OrmService } from './orm.service';
import { PrismaRepository } from './prisma.repository';
import { TypeOrmRepository } from './type-orm.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      MainEntity,
      Sub1Entity,
      Sub2Entity,
      Sub3Entity,
      Sub4Entity,
      Sub5Entity,
    ]),

    PrismaModule,
  ],
  providers: [OrmService, TypeOrmRepository, PrismaRepository],
})
export class OrmModule {}
