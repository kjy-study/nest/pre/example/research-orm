import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../app.module';
import { OrmService } from './orm.service';

jest.setTimeout(28 * 60 * 1000);
describe('OrmService', () => {
  let service: OrmService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    service = module.get<OrmService>(OrmService);
  });

  it('메인 테이블 조회', async () => {
    await checkTime(async () => {
      const data = await service.findOneMain();
      console.log(data);
    }, '메인 테이블 조회');
  });

  it('sub1 테이블 조회', async () => {
    await checkTime(async () => {
      const data = await service.findOneMainRelSub1();
      console.log(data);
    }, 'sub1 테이블 조회');
  });

  it('sub2 테이블 조회', async () => {
    await checkTime(async () => {
      const data = await service.findOneMainRelSub2();
      console.log(data);
    }, 'sub2 테이블 조회');
  });

  it('sub3 테이블 조회', async () => {
    await checkTime(async () => {
      const data = await service.findOneMainRelSub3();
      console.log(data);
    }, 'sub3 테이블 조회');
  });

  it('sub4 테이블 조회', async () => {
    await checkTime(async () => {
      const data = await service.findOneMainRelSub4();
      console.log(data);
    }, 'sub4 테이블 조회');
  });

  it('sub5 테이블 조회', async () => {
    await checkTime(async () => {
      const data = await service.findOneMainRelSub5();
      console.log(data);
    }, 'sub5 테이블 조회');
  });

  async function checkTime(call: any, testName: string) {
    const start = new Date();
    await call();
    const end = new Date();

    console.log(`${testName} - ${end.getTime() - start.getTime()} ms`);
  }
});
