import { Injectable } from '@nestjs/common';
import { PrismaService } from '../config/database/primsa.service';

@Injectable()
export class PrismaRepository {
  constructor(private readonly prismaService: PrismaService) {}

  private readonly dataId = '76f6b046-abb3-11ed-85c8-0242ac190002';

  async findOne() {
    const main = await this.prismaService.mAIN.findUnique({
      where: {
        dataId: this.dataId,
      },
    });

    return main;
  }

  async relSub1() {
    const main = await this.prismaService.mAIN.findUnique({
      where: {
        dataId: this.dataId,
      },
      include: {
        SUB_1: true,
      },
    });

    return main;
  }

  async relSub2() {
    const main = await this.prismaService.mAIN.findUnique({
      where: {
        dataId: this.dataId,
      },
      include: {
        SUB_1: true,
        SUB_2: true,
      },
    });

    return main;
  }

  async relSub3() {
    const main = await this.prismaService.mAIN.findUnique({
      where: {
        dataId: this.dataId,
      },
      include: {
        SUB_1: true,
        SUB_2: true,
        SUB_3: true,
      },
    });

    return main;
  }

  async relSub4() {
    const main = await this.prismaService.mAIN.findUnique({
      where: {
        dataId: this.dataId,
      },
      include: {
        SUB_1: true,
        SUB_2: true,
        SUB_3: true,
        SUB_4: true,
      },
    });

    return main;
  }

  async relSub5() {
    const main = await this.prismaService.mAIN.findUnique({
      where: {
        dataId: this.dataId,
      },
      include: {
        SUB_1: true,
        SUB_2: true,
        SUB_3: true,
        SUB_4: true,
        SUB_5: true,
      },
    });

    return main;
  }
}
