import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { MainEntity } from '../../orm/entities/main.entity';
import { Sub1Entity } from '../../orm/entities/sub-1.entity';

@Injectable()
export class MySqlConfigService implements TypeOrmOptionsFactory {
  constructor(private configService: ConfigService) {}

  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      type: 'mysql',
      username: this.configService.get<string>('database.user'),
      password: this.configService.get<string>('database.password'),
      port: +this.configService.get<number>('database.port'),
      host: this.configService.get<string>('database.host'),
      database: this.configService.get<string>('database.schema'),
      keepConnectionAlive: true, // Cannot create a new connection named "default", because connection with such name already exist and it now has an active connection session
      logging: true,
      autoLoadEntities: true,
      charset: 'utf8mb4',
      // entities: [this.configService.get<string>('typeorm.entityPath')],
      // entities: [MainEntity, Sub1Entity],
      // synchronize: true,
    };
  }
}
