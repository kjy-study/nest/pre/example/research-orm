export default () => ({
  port: process.env.PORT ?? 3000,
  microservice: {
    port: process.env.MICRO_SERVICE_PORT ?? 5001,
  },
  firebase: {
    projectId: process.env.FIREBASE_PROJECT_ID || '',
    clientEmail: process.env.FIREBASE_CLIENT_EMAIL || '',
    privateKey:
      process.env.FIREBASE_PRIVATE_KEY?.split('__n__').join('\n') || '',
  },
  database: {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    host: process.env.DB_HOST,
    schema: process.env.DB_SCHEMA,
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    expires: process.env.JWT_EXPIRES,
    refreshSecret: process.env.JWT_REFRESH_SECRET,
    refreshExpires: process.env.JWT_REFRESH_EXPIRES,
  },
  bcrypt: {
    secretSalt: process.env.SECRET_SALT,
  },
  typeorm: {
    entityPath: process.env.ENTITY_PATH,
  },
  nice: {
    os: process.env.NICE_OS,
    siteCode: process.env.NICE_SITE_CODE,
    sitePw: process.env.NICE_SITE_PW,
    modulePathWin: process.env.NICE_MODULE_PATH_WIN,
    modulePathMac: process.env.NICE_MODULE_PATH_MAC,
    modulePathLinux: process.env.NICE_MODULE_PATH_LINUX,
    modulePathLinux64: process.env.NICE_MODULE_PATH_LINUX_64,
    authType: process.env.NICE_AUTH_TYPE || '',
    customize: process.env.NICE_CUSTOMIZE || '',
    returnUrl: process.env.NICE_RETURN_URL,
    errorUrl: process.env.NICE_ERROR_URL,
    redirectUrl: process.env.NICE_REDIRECT_URL,
  },
  elasticSearch: {
    cloudId: process.env.ELASTIC_CLOUD_ID,
    userName: process.env.ELASTIC_AUTH_USERNAME,
    password: process.env.ELASTIC_AUTH_PASSWORD,
  },
  partnersWeb: {
    url: process.env.PARTNERS_WEB_URL,
  },
});
