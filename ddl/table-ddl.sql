CREATE TABLE MAIN (
  'data_id' char(36) NOT NULL DEFAULT (uuid()),
  'name' varchar(150) DEFAULT NULL,
  PRIMARY KEY (`data_id`)
)
;


CREATE TABLE `SUB_1` (
  `data_id` char(36) NOT NULL DEFAULT (uuid()),
  `main_data_id` char(36) NOT NULL,
  PRIMARY KEY (`data_id`),
  KEY `SUB_1_FK` (`main_data_id`),
  CONSTRAINT `SUB_1_FK` FOREIGN KEY (`main_data_id`) REFERENCES `MAIN` (`data_id`)
);


CREATE TABLE `SUB_2` (
  `data_id` char(36) NOT NULL DEFAULT (uuid()),
  `main_data_id` char(36) NOT NULL,
  PRIMARY KEY (`data_id`),
  KEY `SUB_2_FK` (`main_data_id`),
  CONSTRAINT `SUB_2_FK` FOREIGN KEY (`main_data_id`) REFERENCES `MAIN` (`data_id`)
);

CREATE TABLE `SUB_3` (
  `data_id` char(36) NOT NULL DEFAULT (uuid()),
  `main_data_id` char(36) NOT NULL,
  PRIMARY KEY (`data_id`),
  KEY `SUB_3_FK` (`main_data_id`),
  CONSTRAINT `SUB_3_FK` FOREIGN KEY (`main_data_id`) REFERENCES `MAIN` (`data_id`)
);

CREATE TABLE `SUB_4` (
  `data_id` char(36) NOT NULL DEFAULT (uuid()),
  `main_data_id` char(36) NOT NULL,
  PRIMARY KEY (`data_id`),
  KEY `SUB_4_FK` (`main_data_id`),
  CONSTRAINT `SUB_4_FK` FOREIGN KEY (`main_data_id`) REFERENCES `MAIN` (`data_id`)
);

CREATE TABLE `SUB_5` (
  `data_id` char(36) NOT NULL DEFAULT (uuid()),
  `main_data_id` char(36) NOT NULL,
  PRIMARY KEY (`data_id`),
  KEY `SUB_5_FK` (`main_data_id`),
  CONSTRAINT `SUB_5_FK` FOREIGN KEY (`main_data_id`) REFERENCES `MAIN` (`data_id`)
);

select COUNT(*) from MAIN;
select COUNT(*) from SUB_1;
select COUNT(*) from SUB_2;
select COUNT(*) from SUB_3;
select COUNT(*) from SUB_4;
select COUNT(*) from SUB_5;

insert into MAIN (data_id, name) values ("76f6b046-abb3-11ed-85c8-0242ac190002", "메인정보");

insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_1 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");

insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_2 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");

insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_3 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");

insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_4 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");

insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002"); 
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
insert into SUB_5 (main_data_id) values ("76f6b046-abb3-11ed-85c8-0242ac190002");
