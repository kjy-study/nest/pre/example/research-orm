# Join 성능 테스트
* TypeOrm 0.2 버전 : One-Many 다중 관계를 맺으면 쿼리 질의 시 Left Join의 사용으로  
  굉장히 많은 데이터를 조회하여 **성능 저하**가 발생 됨
* TypeOrm 0.3 버전 : find Option에 `relationLoadStrategy`이 추가되었고, 'query'로 사용하게 되면 Left Join이 아닌 각 테이블마다 select하여 쿼리 질의 속도가 빠름 
* Prisma : 다중 관계 질의 시 성능 저하 없음

## 1. 프로젝트 셋팅

* 패키지 설치

```sh
npm i
```

* .env 작성

```
PORT=3003

# TypeScrit ORM Config  
DB_USER=user
DB_PASSWORD=user1234
DB_PORT=3306
DB_HOST=localhost
DB_SCHEMA=orm
ENTITY_PATH=dist/**/**/*.entity{.js,.ts}

# Prisma Config
DATABASE_URL=mysql://user:user1234@localhost:3306/orm

```

* docker-compose 실행
```sh
docker-compose up -d
```

* DB에 접속하여 ddl 디렉토리의 table-ddl.sql에 있는 스크립트 실행

* prisma
  * migration  
   `npx prisma migrate resolve --applied 0_init`  
   0_init: 마이그레이션 파일 이름
  * client  
   `npx prisma generate`  
   스키마 파일을 기준으로 만들어진 typescript Type을 생성 합니다.

## 2. 테스트

orm.service.spec.ts 파일로 relations 테스트를 진행 합니다.

* git tags
  * typeorm-0.2 : 현재 운영 중인 프로젝트의 typeorm 버전
  * typeorm-0.3 : relations 속도 향상을 위한 테스트 typeorm 버전
  * prisma : Next-generation Node.js and TypeScript ORM