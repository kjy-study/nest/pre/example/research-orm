-- DropForeignKey
ALTER TABLE `SUB_1` DROP FOREIGN KEY `SUB_1_FK`;

-- DropForeignKey
ALTER TABLE `SUB_2` DROP FOREIGN KEY `SUB_2_FK`;

-- DropForeignKey
ALTER TABLE `SUB_3` DROP FOREIGN KEY `SUB_3_FK`;

-- DropForeignKey
ALTER TABLE `SUB_4` DROP FOREIGN KEY `SUB_4_FK`;

-- DropForeignKey
ALTER TABLE `SUB_5` DROP FOREIGN KEY `SUB_5_FK`;

-- AddForeignKey
ALTER TABLE `SUB_1` ADD CONSTRAINT `SUB_1_FK` FOREIGN KEY (`main_data_id`) REFERENCES `MAIN`(`data_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `SUB_2` ADD CONSTRAINT `SUB_2_FK` FOREIGN KEY (`main_data_id`) REFERENCES `MAIN`(`data_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `SUB_3` ADD CONSTRAINT `SUB_3_FK` FOREIGN KEY (`main_data_id`) REFERENCES `MAIN`(`data_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `SUB_4` ADD CONSTRAINT `SUB_4_FK` FOREIGN KEY (`main_data_id`) REFERENCES `MAIN`(`data_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `SUB_5` ADD CONSTRAINT `SUB_5_FK` FOREIGN KEY (`main_data_id`) REFERENCES `MAIN`(`data_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

